/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/12 12:25:55 by rbeaufre          #+#    #+#             */
/*   Updated: 2020/01/12 15:35:12 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COREWAR_H
# define COREWAR_H

# include "../libft/libft.h"
# include "op.h"
# include <fcntl.h>

typedef struct		s_process
{
}					t_process;

typedef struct		s_player
{
	int		nb;
	int		name;
	int		comment;
}					t_player;

typedef struct		s_champ
{
	char	content[CHAMP_MAX_SIZE];
	char	name[PROG_NAME_LENGTH + 4];
	char	comment[COMMENT_LENGTH + 1];
	//char	voider;
	int		champ_nbr;
	int		champ_size;
	int		champ_position;
}			t_champ;

typedef struct		s_cw
{
	t_champ	champ[MAX_PLAYERS];
	int		first_champ_i;
	int		players_count;
	char	arena[MEM_SIZE];
	int		nb_cycles;
	int		nb_processes;
	int		nbr_cycles_to_dump;
	int		dump_flag;
	int		number_flag;
}					t_cw;

int			ft_display_options(void);
int			ft_print_error(char *str);
int			ft_scan_flags(int argc, char **argv, t_cw *cw);
int			ft_init_cw(t_cw *cw);
int			ft_init_champs(int argc, char **argv, t_cw *cw);
int			ft_print_hexa(char *str, int size);

#endif
