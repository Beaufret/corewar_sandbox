/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/03 17:49:40 by rbeaufre          #+#    #+#             */
/*   Updated: 2020/01/12 16:00:40 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/corewar.h"

static int	ft_process_args(int argc, char **argv, t_cw *cw)
{
	cw->first_champ_i = ft_scan_flags(argc, argv, cw);
	ft_init_champs(argc, argv, cw);
	return (1);
}

int			main(int argc, char **argv)
{
	t_cw	cw;

	if (argc == 1 && ft_display_options())
		return (0);
	ft_init_cw(&cw);
	ft_process_args(argc, argv, &cw);
	ft_printf("Le flag dump_flag est fixe a %i\n", cw.dump_flag);
	ft_printf("Le flag number_flag est fixe a %i\n", cw.number_flag);
	ft_printf("{GREEN}Name{EOC}\n");
	ft_print_hexa(cw.champ[0].name, PROG_NAME_LENGTH);
	ft_printf("{GREEN}Comment{EOC}\n");
	ft_print_hexa(cw.champ[0].comment, COMMENT_LENGTH);
	ft_printf("{GREEN}Content{EOC}\n");
	ft_print_hexa(cw.champ[0].content, 23);
	return (0);
}
