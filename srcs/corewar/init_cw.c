/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_cw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/12 13:15:36 by rbeaufre          #+#    #+#             */
/*   Updated: 2020/01/12 14:26:59 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/corewar.h"

int	ft_init_cw(t_cw *cw)
{
	/*cw->arena[MEM_SIZE];
	cw->nb_cycles;
	cw->nb_processes;
	cw->nb_players;
	cw->nbr_cycles_to_dump;
	*/
	cw->dump_flag = 0;
	cw->number_flag = 0;
	cw->players_count = 0;
	return (1);
}
